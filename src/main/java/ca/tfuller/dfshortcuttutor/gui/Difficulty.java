package ca.tfuller.dfshortcuttutor.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum Difficulty {

	PRACTICE("practice", 100000), EASY("baby food", 10000), MEDIUM("noob", 5000), FULLER("fuller",
			1418), PRO("pro coder", 800), PROGRESSIVE("progressive", 10000);

	protected static final Logger logger = LogManager.getLogger(Difficulty.class);
	private String displayName;
	public double secondsPerQuestion;

	Difficulty(String displayName, double secondsPerQuestion) {
		this.displayName = displayName;
		this.secondsPerQuestion = secondsPerQuestion;
	}

	public String getDisplayName() {
		return displayName;
	}

	public double getSecondsPerQuestion() {
		return secondsPerQuestion;
	}

	public void setSecondsPerQuestion(double secondsPerQuestion) {
		this.secondsPerQuestion = secondsPerQuestion;
	}

	@Override
	public String toString() {
		return displayName;
	}

	public static Difficulty parse(String nameString) {
		logger.info("trying to find difficulty name " + nameString);
		for (Difficulty difficulty : values()) {
			if (difficulty.name().equals(nameString)) {
				logger.info("successfully parsed input -->" + nameString + "<-- returning difficulty enum "
						+ difficulty.name());
				return difficulty;
			}
		}
		return null;
	}

	public static Difficulty parse(int i) {
		for (Difficulty difficulty : values()) {
			if (difficulty.ordinal() == i) {
				return difficulty;
			}
		}
		return null;
	}
}
