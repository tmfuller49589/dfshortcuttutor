package ca.tfuller.dfshortcuttutor.gui;

import java.io.IOException;
import java.lang.Runtime.Version;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.server.ErrorMessage;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class DFShortcutTutor extends Application {

	protected static final Logger logger = LogManager.getLogger(DFShortcutTutor.class);

	private Stage primaryStage;
	private AnchorPane rootLayout;
	private DFShortcutTutorController dfShortcutTutorController;
	private static Scene scene;

	@Override
	public void start(Stage priStage) {
		try {

			Version version = java.lang.Runtime.version();
			System.out.println("Java Version = " + version);

			this.primaryStage = priStage;
			this.primaryStage.setTitle("DF Eclise Shortcut Tutor");

			initRootLayout();

		} catch (Exception e) {
			ErrorMessage.display(e);
		}
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DFShortcutTutor.class.getResource("/ca/tfuller/dfshortcuttutor/gui/dfshortcut.fxml"));
			rootLayout = (AnchorPane) loader.load();

			dfShortcutTutorController = (DFShortcutTutorController) loader.getController();

			// Show the scene containing the root layout.
			scene = new Scene(rootLayout);

			primaryStage.setScene(scene);
			primaryStage.show();
			dfShortcutTutorController.setPrimaryStage(primaryStage);

		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	public static void main(String[] args) {

		launch(args);
	}

}
