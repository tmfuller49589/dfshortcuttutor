package ca.tfuller.dfshortcuttutor.gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.server.ErrorMessage;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author tfuller
 *
 */
public class DFShortcutTutorController {
	protected static final Logger logger = LogManager.getLogger(DFShortcutTutorController.class);
	private final List<ShortcutInfo> shortcuts = new ArrayList<ShortcutInfo>();
	private final String filename = "shortcuts.csv";

	private Stage primaryStage;
	private Timeline questionTimer = null;
	private long startTime;
	private double secondsLeft = Difficulty.EASY.secondsPerQuestion;
	private int questionNumber = 0;
	private int correctNumber = 0;
	private boolean gameOn = false;
	private double secondsPerQuestion;

	@FXML
	Label labelScore;

	@FXML
	Label labelShortcut;

	@FXML
	Label labelTimer;

	@FXML
	Label labelTotalTime;

	@FXML
	Label labelProgress;

	@FXML
	Label labelDescription;

	@FXML
	Label labelGameOver;

	@FXML
	Button buttonStart;

	@FXML
	Button buttonStop;

	@FXML
	ComboBox<Difficulty> comboboxDifficulty;

	@FXML
	private void quitClicked() {
		Platform.exit();
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;

		Scene scene = primaryStage.getScene();

		scene.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if (gameOn) {
					logger.info("filter keypressed keycode is " + event.getCode() + " shift: " + event.isShiftDown()
							+ " control: " + event.isControlDown());
					checkKeyPress(event);
					event.consume();
				} else if (event.getCode().equals(KeyCode.ENTER)) {
					event.consume();
					startGame();
				}
			}
		});
	}

	private void checkKeyPress(KeyEvent event) {

		// shortcut that is the current question
		ShortcutInfo shortcut = shortcuts.get(questionNumber);

		if (event.getCode() != KeyCode.SHIFT && event.getCode() != KeyCode.ALT && event.getCode() != KeyCode.CONTROL
				&& event.getCode() != KeyCode.META) {

			if (event.isAltDown() == shortcut.isAlt() && event.isControlDown() == shortcut.isControl()
					&& event.isMetaDown() == shortcut.isMeta() && event.isShiftDown() == shortcut.isShift()
					&& event.getCode().equals(shortcut.getKeyCode())) {
				logger.info("shortcut correct");
				nextQuestion();
			} else {
				logger.info("shortcut not correct");
				if (!comboboxDifficulty.getValue().equals(Difficulty.PRACTICE)) {
					labelGameOver.setText("GAME OVER");
					labelShortcut.setText(shortcut.toString());

					stopGame();
				} else {
					labelGameOver.setText("Try again");
				}
			}
		}
	}

	private void nextQuestion() {
		if (!comboboxDifficulty.getValue().equals(Difficulty.PRACTICE)) {
			questionTimer.stop();

			if (comboboxDifficulty.getValue().equals(Difficulty.PROGRESSIVE)) {
				// drop time per question by 5 percent for progressive difficulty
				secondsPerQuestion *= .95;
			}
			secondsLeft = secondsPerQuestion;

		}
		++questionNumber;
		++correctNumber;

		labelProgress.setText(Integer.toString(questionNumber) + " / " + Integer.toString(shortcuts.size()));
		labelScore.setText(Integer.toString(correctNumber));

		if (!comboboxDifficulty.getValue().equals(Difficulty.PRACTICE)) {
			if (questionNumber == shortcuts.size()) {
				labelGameOver.setText("YOU WIN!!");
				stopGame();
				return;
			}
			questionTimer.playFromStart();
		} else {
			labelGameOver.setText("");

			if (questionNumber == shortcuts.size()) {
				// hit end of list of shortcuts, reset
				Collections.shuffle(shortcuts);
				questionNumber = 0;
			}
		}

		displayQuestion();
	}

	@FXML
	public void initialize() {

		loadQuestions();
		labelTimer.setFont(Font.font(20));
		labelProgress.setFont(Font.font(20));
		labelScore.setFont(Font.font(20));
		comboboxDifficulty.setItems(FXCollections.observableList(List.of(Difficulty.values())));
		comboboxDifficulty.setValue(Difficulty.PRACTICE);
		labelGameOver.setText("");
		labelDescription.setText("");
		labelShortcut.setText("");
	}

	@FXML
	private void startGame() {
		gameOn = true;
		startTime = System.currentTimeMillis();
		buttonStop.setDisable(false);
		buttonStart.setDisable(true);
		secondsPerQuestion = comboboxDifficulty.getValue().secondsPerQuestion;
		displayTime();
		correctNumber = 0;
		secondsLeft = secondsPerQuestion;
		if (!comboboxDifficulty.getValue().equals(Difficulty.PRACTICE)) {
			questionTimer = new Timeline(new KeyFrame(Duration.millis(100), new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					secondsLeft -= 100;
					displayTime();
					if (secondsLeft == 0) {
						stopGame();
						labelGameOver.setText("GAME OVER");
					}
				}
			}));
			questionTimer.setCycleCount(Animation.INDEFINITE);
			questionTimer.play();
		} else {
			questionTimer = null;
		}

		Collections.shuffle(shortcuts);
		questionNumber = 0;
		labelGameOver.setText("");
		labelShortcut.setText("");
		comboboxDifficulty.setDisable(true);
		displayQuestion();
	}

	private void displayQuestion() {
		labelGameOver.setText("");
		ShortcutInfo shortcut = shortcuts.get(questionNumber);
		labelDescription.setText(shortcut.getDescription());
		if (comboboxDifficulty.getValue().equals(Difficulty.PRACTICE)) {
			labelShortcut.setText(shortcuts.get(questionNumber).toString());
		}
	}

	private void displayTime() {
		if (secondsLeft > 0) {
			DecimalFormat df = new DecimalFormat("###.##");
			labelTimer.setText(df.format(secondsLeft / 1000.0));
			labelTotalTime.setText(df.format((System.currentTimeMillis() - startTime) / 1000.0));
		} else {
			labelTimer.setText("time is up");
		}
	}

	@FXML
	private void stopGame() {
		logger.info("stopping game");
		gameOn = false;
		comboboxDifficulty.setDisable(false);
		buttonStart.setDisable(false);
		buttonStop.setDisable(true);
		if (questionTimer != null) {
			questionTimer.stop();
			logger.info("stopping questionTimer " + questionTimer);
		}
	}

	private void loadQuestions() {
		InputStream is = DFShortcutTutor.class.getClassLoader().getResourceAsStream(filename);

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
			while (reader.ready()) {
				String line = reader.readLine();
				String[] tokens = line.split(",");
				if (tokens.length == 6) {
					ShortcutInfo sci = new ShortcutInfo(tokens);
					shortcuts.add(sci);
				}
			}

		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	private void printShortcuts() {
		logger.info("printing shortcuts");
		for (ShortcutInfo s : shortcuts) {
			logger.info(s);
		}
	}
}
