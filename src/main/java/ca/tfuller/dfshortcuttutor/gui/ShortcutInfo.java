package ca.tfuller.dfshortcuttutor.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.input.KeyCode;

public class ShortcutInfo {

	private String keyname;
	private String description;
	private boolean shift;
	private boolean control;
	private boolean alt;
	private boolean meta;
	private KeyCode keyCode;
	protected static final Logger logger = LogManager.getLogger(ShortcutInfo.class);

	public ShortcutInfo(String[] values) {
		control = parseBoolean(values[0]);
		shift = parseBoolean(values[1]);
		alt = parseBoolean(values[2]);
		meta = parseBoolean(values[3]);
		keyname = values[4].trim();
		description = values[5].trim();
		setKeyCode();
	}

	/*
	 * Turn "0" into false, and "1" into true
	 */
	private boolean parseBoolean(String intString) {
		if (intString.equals("0")) {
			return false;
		}
		return true;
	}

	public ShortcutInfo(boolean shift, boolean control, boolean alt, boolean meta, String keyname, String description) {
		super();
		this.keyname = keyname;
		this.description = description;
		this.shift = shift;
		this.control = control;
		this.alt = alt;
		this.meta = meta;
		setKeyCode();
	}

	private void setKeyCode() {
		keyCode = KeyCode.valueOf(keyname);
	}

	public boolean isShift() {
		return shift;
	}

	public void setShift(boolean shift) {
		this.shift = shift;
	}

	public boolean isControl() {
		return control;
	}

	public void setControl(boolean control) {
		this.control = control;
	}

	public boolean isAlt() {
		return alt;
	}

	public void setAlt(boolean alt) {
		this.alt = alt;
	}

	public boolean isMeta() {
		return meta;
	}

	public void setMeta(boolean meta) {
		this.meta = meta;
	}

	public String getKeyname() {
		return keyname;
	}

	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		String s = "";
		logger.info("control is " + control);
		if (shift) {
			s += "SHIFT ";
		}
		if (alt) {
			s += "ALT ";
		}
		if (meta) {
			s += "META ";
		}
		if (control) {
			s += "CTRL ";
		}
		s += keyname;
		return s;
	}

	public KeyCode getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(KeyCode keyCode) {
		this.keyCode = keyCode;
	}

}
