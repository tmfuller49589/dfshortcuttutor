package ca.tfuller.dfshortcuttutor.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.server.Player;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * @author tfuller
 *
 */
public class PlayerInitController {
	protected static final Logger logger = LogManager.getLogger(PlayerInitController.class);

	private Stage primaryStage;

	@FXML
	AnchorPane anchorPanePlayerInit;

	@FXML
	GridPane gridPaneIcon;

	@FXML
	TextField textFieldPlayerName;

	@FXML
	Button buttonNext;

	List<Button> iconButtonList = new ArrayList<Button>();

	protected boolean iconClicked;
	private String iconFileName = "";

	private static ClientController clientController;

	public static void setClientController(ClientController clientController) {
		PlayerInitController.clientController = clientController;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;

		Scene scene = primaryStage.getScene();

	}

	@FXML
	public void initialize() {
		initializeIcons();
	}

	private void initializeIcons() {
		for (int row = 0; row < 4; ++row) {
			for (int col = 0; col < 4; ++col) {
				int i = row * 4 + col + 1;
				Button but = new Button();
				String filename = "dino" + i + ".png";

				Image image = new Image(PlayerInitController.class.getClassLoader().getResourceAsStream(filename));

				ImageView imView = new ImageView(image);
				imView.setFitHeight(70);
				imView.setFitWidth(70);
				imView.setPreserveRatio(true);
				but.setPrefSize(75, 75);
				but.setUserData(filename);
				iconButtonList.add(but);
				imView.fitWidthProperty().bind(but.widthProperty());
				//imView.fitHeightProperty().bind(but.heightProperty());

				but.setGraphic(imView);
				but.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						Button butt = (Button) event.getSource();
						iconFileName = (String) butt.getUserData();
						iconClicked = true;
						if (!textFieldPlayerName.getText().isEmpty()) {
							buttonNext.setDisable(false);
						} else {
							buttonNext.setDisable(true);
						}

					}
				});

				gridPaneIcon.add(but, col, row);
			}
		}
	}

	List<Button> getIconButtonList() {
		return iconButtonList;
	}

	void refreshPlayerIcons() {

		// disable button if any player has already chosen the icon
		// enable button if no player has chosen the icon
		for (Button b : iconButtonList) {
			boolean iconTaken = false;
			for (Entry<Integer, Player> entry : clientController.getPlayers().entrySet()) {
				Player p = entry.getValue();
				if (((String) b.getUserData()).equals(p.getIconFileName())) {
					b.setDisable(true);
					iconTaken = true;
					break;
				}
			}
			if (iconTaken == false) {
				b.setDisable(false);
			}
		}
	}

	@FXML
	private void nextClicked() {
		logger.info("next clicked");
		clientController.requestInitializePlayer(textFieldPlayerName.getText(), iconFileName);
	}

	@FXML
	private void playerNameEntered() {
		if (!textFieldPlayerName.getText().isEmpty() && iconClicked) {
			buttonNext.setDisable(false);
		} else {
			buttonNext.setDisable(true);
		}
	}
}
