package ca.tfuller.dfshortcuttutor.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.gui.Difficulty;
import ca.tfuller.dfshortcuttutor.server.CommActionClientSend;
import ca.tfuller.dfshortcuttutor.server.CommActionServerSend;
import ca.tfuller.dfshortcuttutor.server.ErrorMessage;
import ca.tfuller.dfshortcuttutor.server.Game;
import ca.tfuller.dfshortcuttutor.server.Player;
import ca.tfuller.dfshortcuttutor.server.ServerController;
import javafx.application.Platform;

public class SocketSelectorReadWriter {
	protected static final Logger logger = LogManager.getLogger(SocketSelectorReadWriter.class);

	private Selector selector;
	private String host;
	private int port;
	private ClientController clientController;
	private SocketChannel channel;
	private boolean shutdown = false;
	private StringBuilder bufStr = new StringBuilder();
	private ByteBuffer readBuffer;
	private ByteBuffer writeBuffer;

	public SocketSelectorReadWriter(ClientController clientController, String host, int port) {
		this.host = host;
		this.port = port;
		writeBuffer = ByteBuffer.allocate(ServerController.BUFFERSIZE);
		readBuffer = ByteBuffer.allocate(ServerController.BUFFERSIZE);
		this.clientController = clientController;
	}

	public boolean processReadySet(Set<SelectionKey> readySet) throws Exception {
		Iterator<SelectionKey> iterator = readySet.iterator();
		while (iterator.hasNext()) {
			SelectionKey key = iterator.next();
			iterator.remove();
			if (key.isConnectable()) {
				logger.info("key.isConnectable triggered");
				boolean connected = processConnect(key);
				if (!connected) {
					return true; // Exit
				}
			}
			if (key.isReadable()) {
				logger.info("key.isReadable triggered");
				processRead(key);
			}
		}
		return false; // Not done yet
	}

	public synchronized void send(String message) {
		try {
			if (message.equals(CommActionClientSend.CLIENTSHUTDOWN.name())) {
				shutdown = true;
			}
			writeBuffer.clear();
			message += " " + clientController.getPlayer().getId() + " " + ServerController.EOM;
			logger.info("sending message -->" + message + "<--");
			writeBuffer = ByteBuffer.wrap(message.getBytes());

			channel.write(writeBuffer);

			final String msg = message;
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					clientController.displayTransmit(msg);
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
			ErrorMessage.display(e);
		}
		writeBuffer.clear();

	}

	private boolean processConnect(SelectionKey key) throws Exception {
		SocketChannel keyChannel = (SocketChannel) key.channel();
		while (keyChannel.isConnectionPending()) {
			keyChannel.finishConnect();
		}

		keyChannel.register(selector, SelectionKey.OP_READ);
		key.interestOps(SelectionKey.OP_READ);

		logger.info("socket connection established, sending CommActionClient.CONNECT");
		send(CommActionClientSend.CONNECT.name());

		return true;
	}

	public void shutdown() {
		send(CommActionClientSend.CLIENTSHUTDOWN.name());
	}

	private void processRead(SelectionKey key) throws Exception {
		SocketChannel sChannel = (SocketChannel) key.channel();
		readBuffer.clear();
		logger.info("attempting read");
		int bytesRead = sChannel.read(readBuffer);

		logger.info("bytes read: " + bytesRead);
		if (bytesRead == -1) {
			return;
		}

		String str = new String(readBuffer.array());
		logger.info("read -->" + str + "<--");
		str = str.trim();
		bufStr.append(new String(readBuffer.array()).trim().substring(0, Math.min(bytesRead, str.length())));
		logger.info("processRead received from server -->" + bufStr.toString() + "<--");

		// split off chunks up to the first EOM and process
		while (bufStr.toString().contains(ServerController.EOM)) {
			String message = bufStr.toString().substring(0, bufStr.toString().indexOf(ServerController.EOM));
			logger.info("splitting at EOM-->" + message + "<--");
			processMessage(message);

			final String msg = message;
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					clientController.displayReceived(msg);
				}
			});

			bufStr.delete(0, bufStr.toString().indexOf(ServerController.EOM) + 3);

		}
	}

	void start() {
		try {
			InetSocketAddress serverAddress = new InetSocketAddress(host, port);
			selector = Selector.open();
			channel = SocketChannel.open();
			channel.configureBlocking(false);
			channel.connect(serverAddress);

			int operations = SelectionKey.OP_CONNECT;
			channel.register(selector, operations);

			while (!shutdown) {
				logger.info("calling selector.select");
				int numSelected = selector.select();
				logger.info("number of selected channels = " + numSelected);
				if (numSelected > 0) {
					logger.info("selector triggered, processing");
					processReadySet(selector.selectedKeys());
				}
			}
			logger.info("exited read/write loop, closing channel");
			channel.close();
		} catch (IOException e) {
			e.printStackTrace();
			ErrorMessage.display(e);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorMessage.display(e);
		}
	}

	/**
	 * @param message
	 */
	private void processMessage(String message) {
		CommActionServerSend cas = CommActionServerSend.parseWithParameters(message);

		logger.info("CommActionServer: -->" + cas.toString() + "<--");
		switch (cas) {
		case CONNECTED:
			logger.info("connected, " + cas.getParameter(0));
			int playerId = Integer.parseInt(cas.getParameter(0));
			logger.info("got player id " + playerId + " from server");
			clientController.setPlayerId(playerId);
			logger.info("back from clientControl.setPlayerId()");
			send(CommActionClientSend.GETPLAYERLIST.name());
			break;
		case TERMINATE:
			break;
		case GAMECREATED:
			logger.info("game created message received");
			int gameId = Integer.parseInt(cas.getParameter(0));
			logger.info("gameId is " + gameId);
			int ownerId = Integer.parseInt(cas.getParameter(1));
			Difficulty difficulty = Difficulty.parse(cas.getParameter(2));
			logger.info("difficulty is " + difficulty);
			clientController.gameCreated(gameId, ownerId, difficulty);
			break;
		case SETPLAYERINFO:
			playerId = Integer.parseInt(cas.getParameter(0));
			String playerName = cas.getParameter(1);
			String iconFileName = cas.getParameter(2);
			clientController.setPlayerInfo(playerId, playerName, iconFileName);
			break;
		case GAMELIST:
			String tmp = message.replace(CommActionServerSend.GAMELIST.name(), "");
			// if there are no games in the message from server,
			// EOL will not appear in the message
			if (tmp.contains(ServerController.EOL)) {
				String[] gameLines = tmp.trim().split(ServerController.EOL);
				for (String gameLine : gameLines) {
					logger.info("gameLine-->" + gameLine + "<--");
					String[] token = gameLine.split(" ");
					for (int i = 0; i < token.length; ++i) {
						logger.info("token >>>" + token[i] + "<<<");
					}
					gameId = Integer.parseInt(token[0]);
					String gameName = token[1];
					ownerId = Integer.parseInt(token[2]);
					difficulty = Difficulty.parse(token[3]);
					Player owner = clientController.getPlayers().get(ownerId);
					Game game = new Game(gameId, owner, gameName, difficulty);

					// get list of players of the game
					int i = 4;
					while (i < token.length && !token[i].equals(ServerController.EOM)) {
						playerId = Integer.parseInt(token[i]);
						++i;
						game.getPlayers().add(clientController.getPlayers().get(playerId));
					}
					clientController.getGames().add(game);
				}
			}
			break;
		case PLAYERLIST:
			tmp = message.replace(CommActionServerSend.PLAYERLIST.name(), "");
			String[] tokens = tmp.trim().split(" ");
			for (String token : tokens) {
				logger.info("-->" + token + "<--");
			}
			int i = 0;
			while (i < tokens.length) {
				if (tokens[i] != null && tokens[i].equals(ServerController.EOM)) {
					break;
				}
				playerId = Integer.parseInt(tokens[i]);
				++i;
				playerName = tokens[i];
				++i;
				iconFileName = tokens[i];
				++i;

				clientController.addPlayer(playerId, playerName, iconFileName);
			}
			clientController.refreshPlayerIcons();
			logger.info("finished processing player list");
			break;
		case JOINEDGAME:
			// JOINEDGAME gameid playerid
			gameId = Integer.parseInt(cas.getParameter(0));
			playerId = Integer.parseInt(cas.getParameter(1));
			playerId = Integer.parseInt(cas.getParameter(1));
			clientController.joinedGame(gameId, playerId);
			break;
		case LEFTGAME:
			// LEFTGAME gameid playerid
			gameId = Integer.parseInt(cas.getParameter(0));
			playerId = Integer.parseInt(cas.getParameter(1));
			clientController.leftGame(gameId, playerId);
			break;
		case STARTEDGAME:
			final int gameIdToRun = Integer.parseInt(cas.getParameter(0));

			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					// updates UI, have to call via runLater
					clientController.startGame(gameIdToRun);
				}
			});

			break;
		case DIFFICULTYSET:
			clientController.setGameDifficulty(Integer.parseInt(cas.getParameter(0)),
					Difficulty.parse(cas.getParameter(1)));
			break;
		case PLAYERREMOVED:
			clientController.playerRemoved(Integer.parseInt(cas.getParameter(0)));
			break;
		case PLAYERADVANCED:
			clientController.playerAdvanced(Integer.parseInt(cas.getParameter(0)));
			break;
		case GAMEDELETED:
			clientController.deleteGame(Integer.parseInt(cas.getParameter(0)));
			break;
		case PING:
			message = CommActionClientSend.PONG + " ";
			send(message);
			break;
		case PLAYERWON:
			logger.info("PLAYERWON");
			gameId = Integer.parseInt(cas.getParameter(0));
			if (clientController.getRunningGame().getId() == gameId) {
				playerId = Integer.parseInt(cas.getParameter(1));
				clientController.playerWon(gameId, playerId);
			}
			break;
		case SHUTDOWN:
			break;
		default:
			break;
		}
	}
}
