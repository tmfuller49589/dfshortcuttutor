package ca.tfuller.dfshortcuttutor.client;

import java.io.IOException;
import java.lang.Runtime.Version;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.server.ErrorMessage;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

// TODO remove started games from available game list

public class Client extends Application {

	protected static final Logger logger = LogManager.getLogger(Client.class);

	private Stage primaryStage;
	private AnchorPane rootLayout;
	private GameController gameController;
	private ClientController clientController;
	private ConnectToServerController connectToServerController;
	private static Scene scene;

	@Override
	public void start(Stage priStage) {
		try {

			Version version = java.lang.Runtime.version();
			System.out.println("Java Version = " + version);

			this.primaryStage = priStage;
			this.primaryStage.setTitle("DF Eclipse Shortcut Tutor");

			initRootLayout();

		} catch (Exception e) {
			ErrorMessage.display(e);
		}
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Client.class.getResource("/ca/tfuller/dfshortcuttutor/client/client.fxml"));
			rootLayout = (AnchorPane) loader.load();

			clientController = (ClientController) loader.getController();

			// Show the scene containing the root layout.
			scene = new Scene(rootLayout);

			primaryStage.setScene(scene);
			primaryStage.show();
			clientController.setPrimaryStage(primaryStage);

			loader = new FXMLLoader();
			loader.setLocation(Client.class.getResource("/ca/tfuller/dfshortcuttutor/client/connectToServer.fxml"));
			AnchorPane ap = (AnchorPane) loader.load();
			connectToServerController = (ConnectToServerController) loader.getController();
			clientController.setContent(ap);
			clientController.setConnectToServerController(connectToServerController);

		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	@Override
	public void stop() {
		clientController.shutdown();
	}

	public static void main(String[] args) {

		launch(args);
	}

}
