package ca.tfuller.dfshortcuttutor.client;

import java.text.DecimalFormat;
import java.util.Random;

public class Garbage {
	public static void main(String[] args) {
		Random rgen = new Random();
		DecimalFormat df = new DecimalFormat("0.000");
		double[] dds = new double[10];

		for (int i = 0; i < dds.length; ++i) {
			dds[i] = rgen.nextDouble() * 100;
		}

		for (double i : dds) {
			System.out.println(df.format(i));
		}
	}
}
