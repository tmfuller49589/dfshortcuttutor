package ca.tfuller.dfshortcuttutor.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.gui.Difficulty;
import ca.tfuller.dfshortcuttutor.gui.ShortcutInfo;
import ca.tfuller.dfshortcuttutor.server.Game;
import ca.tfuller.dfshortcuttutor.server.GameStatus;
import ca.tfuller.dfshortcuttutor.server.Player;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * @author tfuller
 *
 */
public class GameController {
	protected static final Logger logger = LogManager.getLogger(GameController.class);
	private static ClientController clientController;

	private Stage primaryStage;
	private long startTime;
	private int questionNumber = 0;
	private int correctNumber = 0;
	private Map<Integer, ImageView> imageViewMap = new HashMap<Integer, ImageView>();

	@FXML
	Label labelScore;

	@FXML
	Label labelShortcut;

	@FXML
	Label labelTotalTime;

	@FXML
	Label labelProgress;

	@FXML
	Label labelDescription;

	@FXML
	Label labelGameOver;

	@FXML
	Button buttonStop;

	@FXML
	Pane panePlayers;

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;

		Scene scene = primaryStage.getScene();

		scene.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if (clientController.getRunningGame().getStatus() == GameStatus.RUNNING) {
					logger.info("filter keypressed keycode is " + event.getCode() + " shift: " + event.isShiftDown()
							+ " control: " + event.isControlDown());
					checkKeyPress(event);
					event.consume();
				}
			}
		});
	}

	private void renderPlayers() {
		Game game = clientController.getRunningGame();
		int padding = 5;

		int playerNumber = 0;
		for (Player player : game.getPlayers()) {
			ImageView imview = imageViewMap.get(player.getId());
			double x = (panePlayers.getWidth() - padding) / game.getNumberOfQuestions() * player.getNumCorrect()
					+ padding;
			double y = (panePlayers.getHeight() - padding) / game.getPlayers().size() * playerNumber + padding;
			++playerNumber;
			imview.relocate(x, y);
		}
	}

	private void checkKeyPress(KeyEvent event) {

		// shortcut that is the current question
		ShortcutInfo shortcut = clientController.getRunningGame().getShortcuts().get(questionNumber);

		if (event.getCode() != KeyCode.SHIFT && event.getCode() != KeyCode.ALT && event.getCode() != KeyCode.CONTROL
				&& event.getCode() != KeyCode.META) {

			if (event.isAltDown() == shortcut.isAlt() && event.isControlDown() == shortcut.isControl()
					&& event.isMetaDown() == shortcut.isMeta() && event.isShiftDown() == shortcut.isShift()
					&& event.getCode().equals(shortcut.getKeyCode())) {
				logger.info("shortcut correct");
				nextQuestion();
			} else {
				logger.info("shortcut not correct");
				logger.info("difficulty is " + clientController.getRunningGame().getDifficulty().name());
				if (!clientController.getRunningGame().getDifficulty().equals(Difficulty.PRACTICE)) {
					labelGameOver.setText("GAME OVER");
					labelShortcut.setText(shortcut.toString());

					stopGame();
				} else {
					labelGameOver.setText("Try again");
				}
			}
		}
	}

	private void nextQuestion() {

		++questionNumber;
		++correctNumber;
		clientController.requestPlayerAdvance();

		labelProgress.setText(Integer.toString(questionNumber) + " / "
				+ Integer.toString(clientController.getRunningGame().getNumberOfQuestions()));
		labelScore.setText(Integer.toString(correctNumber));

		if (!clientController.getRunningGame().getDifficulty().equals(Difficulty.PRACTICE)) {
			if (questionNumber == clientController.getRunningGame().getShortcuts().size()) {
				labelGameOver.setText("YOU WIN!!");
				stopGame();
				return;
			}
		} else {
			labelGameOver.setText("");

			if (questionNumber == clientController.getRunningGame().getShortcuts().size()) {
				// hit end of list of shortcuts, reset
				Collections.shuffle(clientController.getRunningGame().getShortcuts());
				questionNumber = 0;
			}
		}

		displayQuestion();
	}

	@FXML
	public void initialize() {

		labelProgress.setFont(Font.font(20));
		labelScore.setFont(Font.font(20));

		labelGameOver.setText("");
		labelDescription.setText("");
		labelShortcut.setText("");

	}

	@FXML
	protected void startGame(Game game2) {
		for (Player player : clientController.getRunningGame().getPlayers()) {
			Image image = new Image(
					GameChooserController.class.getClassLoader().getResourceAsStream(player.getIconFileName()));
			ImageView imView = new ImageView(image);
			imView.setFitHeight(70);
			imView.setFitWidth(70);
			imView.setPreserveRatio(true);
			panePlayers.getChildren().add(imView);
			imageViewMap.put(player.getId(), imView);
		}

		startTime = System.currentTimeMillis();
		buttonStop.setDisable(false);
		correctNumber = 0;

		Collections.shuffle(clientController.getRunningGame().getShortcuts());
		questionNumber = 0;
		labelGameOver.setText("");
		labelShortcut.setText("");

		displayQuestion();
	}

	private void displayQuestion() {
		labelGameOver.setText("");
		ShortcutInfo shortcut = clientController.getRunningGame().getShortcuts().get(questionNumber);
		labelDescription.setText(shortcut.getDescription());
		if (clientController.getRunningGame().getDifficulty().equals(Difficulty.PRACTICE)) {
			labelShortcut.setText(clientController.getRunningGame().getShortcuts().get(questionNumber).toString());
		}
	}

	@FXML
	private void stopGame() {
		logger.info("stopping game");
		buttonStop.setDisable(true);
	}

	public static void setClientController(ClientController clientController) {
		GameController.clientController = clientController;
	}

	public void playerAdvanced(int playerId) {
		logger.info("advancing player " + playerId);
		Game game = clientController.getRunningGame();
		game.advancePlayer(playerId);

		renderPlayers();
		checkForWinner();
	}

	private void checkForWinner() {
		Game game = clientController.getRunningGame();
		if (game.hasWinner()) {
			clientController.requestPlayerWin(game.getId(), game.getWinner().getId());
		}
	}

	public void playerWon(int gameId, int playerId) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Game game = clientController.getRunningGame();
				List<Player> winners = new ArrayList<Player>();
				final StringBuilder winnersString = new StringBuilder();
				winnersString.append("Winners:" + System.lineSeparator());
				for (Player player : game.getPlayers()) {
					logger.info("player " + player.getName() + " score: " + player.getNumCorrect() + " / "
							+ game.getNumberOfQuestions());
					if (player.getNumCorrect() == game.getNumberOfQuestions()) {
						winners.add(player);
						winnersString.append(player.getName() + " score: " + +player.getNumCorrect() + " / "
								+ game.getNumberOfQuestions() + System.lineSeparator());
					}
				}
				labelGameOver.setText(winnersString.toString());
			}
		});
	}
}
