package ca.tfuller.dfshortcuttutor.client;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.gui.Difficulty;
import ca.tfuller.dfshortcuttutor.server.Game;
import ca.tfuller.dfshortcuttutor.server.GameStatus;
import ca.tfuller.dfshortcuttutor.server.Player;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * @author tfuller
 *
 */
public class GameChooserController {
	protected static final Logger logger = LogManager.getLogger(GameChooserController.class);

	private Stage primaryStage;

	@FXML
	AnchorPane anchorPaneGameChooser;

	@FXML
	ListView<Game> listViewGames;

	@FXML
	Button buttonStart;

	@FXML
	Button buttonJoinGame;

	@FXML
	Button buttonLeaveGame;

	@FXML
	Button buttonCreateGame;

	@FXML
	Button buttonDeleteGame;

	@FXML
	ComboBox<Difficulty> comboboxDifficulty;

	private static ClientController clientController;

	static void setClientController(ClientController clientController) {
		GameChooserController.clientController = clientController;
	}

	void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;

		Scene scene = primaryStage.getScene();

	}

	@FXML
	public void initialize() {

		comboboxDifficulty.setItems(FXCollections.observableList(List.of(Difficulty.values())));
		comboboxDifficulty.setValue(Difficulty.PRACTICE);

		listViewGames.setCellFactory(param -> new ListCell<Game>() {
			@Override
			protected void updateItem(Game game, boolean empty) {
				super.updateItem(game, empty);

				if (empty || game == null || game.getName() == null || game.getStatus() != GameStatus.CREATED) {
					setText(null);
					setGraphic(null);
				} else {
					HBox hbox = new HBox();
					String filename = game.getOwner().getIconFileName();
					logger.info("icon file name is " + filename);
					logger.info("owner name is " + game.getOwner().getName());
					setText(game.getName() + " " + game.getOwner().getName());

					for (Player player : game.getPlayers()) {
						logger.info("listview adding player " + player.getName() + " to game " + game.getId());
						Image image = new Image(GameChooserController.class.getClassLoader()
								.getResourceAsStream(player.getIconFileName()));
						ImageView imView = new ImageView(image);
						imView.setFitHeight(70);
						imView.setFitWidth(70);
						imView.setPreserveRatio(true);
						hbox.getChildren().add(imView);
					}

					setGraphic(hbox);
				}
			}
		});

		listViewGames.setItems(clientController.getGames());

		listViewGames.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Game>() {
			@Override
			// disable join button if user selected the game that they created
			public void changed(ObservableValue<? extends Game> observable, Game oldGame, Game newGame) {
				if (newGame != null) {
					logger.info("new game owner is " + newGame.getOwner().getName() + " " + newGame.getOwner().getId());
					logger.info("this player is " + clientController.getPlayer().getId());
				}
			}
		});

		refreshGameList();
	}

	/*
	 * Called when user clicks start game button
	 */
	@FXML
	private void startGame() {
		logger.info("start game");
		buttonStart.setDisable(true);
		Game game = clientController.getOwnerGame();
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Start game?");
		String content = "You can only start the game that you created." + System.lineSeparator();
		content += "Game name: " + game.getName() + System.lineSeparator();
		content += "Game id: " + game.getId() + System.lineSeparator();
		content += "Players: " + System.lineSeparator();
		content += "   game owner name: " + game.getOwner().getName() + System.lineSeparator();
		content += "   game owner id: " + game.getOwner().getId() + System.lineSeparator();
		for (Player player : game.getPlayers()) {
			content += "    player name: " + player.getName() + System.lineSeparator();
			content += "    player id: " + player.getId() + System.lineSeparator();
		}
		alert.setContentText(content);
		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			clientController.requestStartGame();
		}

	}

	@FXML
	private void createGame() {
		logger.info("create game");
		// send server create game request
		clientController.requestCreateGame(comboboxDifficulty.getSelectionModel().getSelectedItem());
		buttonCreateGame.setDisable(true);
	}

	/**
	 * User clicked delete game.
	 */
	@FXML
	private void deleteGame() {
		logger.info("delete game");
		buttonDeleteGame.setDisable(true);
		// send server delete game request
		clientController.requestDeleteGame();
	}

	/*
	 * User clicked join game button.
	 * Button will alternate between join and leave depending on
	 * whether user has already joined selected game.
	 */
	@FXML
	private void joinGame() {
		logger.info("join game");
		buttonJoinGame.setDisable(true);
		// send server join game request
		if (listViewGames.getSelectionModel().getSelectedItem() != null) {
			Game selectedGame = listViewGames.getSelectionModel().getSelectedItem();
			clientController.requestJoinGame(selectedGame);
		}
	}

	@FXML
	private void leaveGame() {
		buttonLeaveGame.setDisable(true);
		clientController.requestLeaveGame();
	}

	@FXML
	void setDifficulty() {
		clientController.requestSetGameDifficulty(comboboxDifficulty.getSelectionModel().getSelectedItem());
	}

	void refreshGameList() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				logger.info("refreshing UI, size of games list: " + clientController.getGames().size());
				listViewGames.refresh();

				boolean hasCreatedGame = false;
				if (clientController.getOwnerGame() != null) {
					hasCreatedGame = true;
				}
				boolean hasJoinedGame = clientController.hasJoinedGame();

				if (hasCreatedGame && hasJoinedGame) {
					buttonJoinGame.setDisable(true);
					buttonCreateGame.setDisable(true);
					buttonDeleteGame.setDisable(false);
					buttonLeaveGame.setDisable(true);
					comboboxDifficulty.setDisable(false);
					buttonStart.setDisable(false);
				} else if (!hasCreatedGame && hasJoinedGame) {
					buttonJoinGame.setDisable(true);
					buttonCreateGame.setDisable(true);
					buttonDeleteGame.setDisable(true);
					buttonLeaveGame.setDisable(false);
					comboboxDifficulty.setDisable(true);
					buttonStart.setDisable(true);
				} else if (hasCreatedGame && !hasJoinedGame) {
					// will not occur
				} else if (!hasCreatedGame && !hasJoinedGame) {
					buttonJoinGame.setDisable(false);
					buttonCreateGame.setDisable(false);
					buttonDeleteGame.setDisable(true);
					buttonLeaveGame.setDisable(true);
					comboboxDifficulty.setDisable(true);
					buttonStart.setDisable(true);
				}
			}
		});
	}
}
