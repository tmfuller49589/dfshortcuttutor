package ca.tfuller.dfshortcuttutor.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.server.ErrorMessage;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ConnectToServerController {
	protected static final Logger logger = LogManager.getLogger(ConnectToServerController.class);

	private Stage primaryStage;

	@FXML
	AnchorPane anchorPaneConnectToServer;

	@FXML
	Button buttonConnectServer;

	@FXML
	GridPane gridPaneIcon;

	@FXML
	TextField textFieldServerIP;

	@FXML
	TextField textFieldServerPort;

	private static ClientController clientController;
	public static final String lastIpFile = "lastServerIpUsed.txt";

	public static void setClientController(ClientController clientController) {
		ConnectToServerController.clientController = clientController;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;

		Scene scene = primaryStage.getScene();

	}

	@FXML
	public void initialize() {
		File file = new File(lastIpFile);
		if (file.exists()) {
			Path path = file.toPath();
			try {
				List<String> lines = Files.readAllLines(path);
				String ipAddress = lines.get(0);
				logger.info("read " + ipAddress + " from " + lastIpFile);
				textFieldServerIP.setText(ipAddress);
			} catch (IOException e) {
				ErrorMessage.display(e);
			}
		}
	}

	@FXML
	private void connectToServer() {
		clientController.connectToServer(textFieldServerIP.getText(), Integer.parseInt(textFieldServerPort.getText()));

		File file = new File(lastIpFile);
		try {
			PrintWriter pw = new PrintWriter(file);
			pw.print(textFieldServerIP.getText());
			pw.close();
		} catch (FileNotFoundException e) {
			ErrorMessage.display(e);
		}
	}

}
