/*
 * 
 */
package ca.tfuller.dfshortcuttutor.client;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.gui.Difficulty;
import ca.tfuller.dfshortcuttutor.server.CommActionClientSend;
import ca.tfuller.dfshortcuttutor.server.ErrorMessage;
import ca.tfuller.dfshortcuttutor.server.Game;
import ca.tfuller.dfshortcuttutor.server.GameStatus;
import ca.tfuller.dfshortcuttutor.server.Player;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * The Class ClientController.
 *
 * @author tfuller
 */
/**
 * 
 */
/**
 * 
 */
/**
 * 
 */
public class ClientController {

	protected static final Logger logger = LogManager.getLogger(ClientController.class);

	private SocketSelectorReadWriter socketReadWriter;

	private ExecutorService executor;

	private SocketChannel clientChannel;

	private Stage primaryStage;

	private Player player = new Player();

	private ObservableList<Game> games = FXCollections.observableArrayList();

	private Game runningGame = null;

	private Map<Integer, Player> players = new HashMap<Integer, Player>();

	private PlayerInitController playerInitController;

	private GameChooserController gameChooserController;

	private GameController gameController;

	private ConnectToServerController connectToServerController;

	private Runnable readWriterThread;

	/** The anchor pane main. */
	@FXML
	AnchorPane anchorPaneCenter;

	private List<String> availableIcons = new ArrayList<String>();

	@FXML
	private TextArea textAreaReceive;
	@FXML
	private TextArea textAreaTransmit;

	/**
	 * Sets the primary stage.
	 *
	 * @param primaryStage
	 *            the new primary stage
	 */
	void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;

		Scene scene = primaryStage.getScene();

	}

	/**
	 * Initialize.
	 */
	@FXML
	void initialize() {
		GameController.setClientController(this);
		ConnectToServerController.setClientController(this);
		PlayerInitController.setClientController(this);
		GameChooserController.setClientController(this);

		for (int i = 1; i <= 16; ++i) {
			String filename = "dino" + i + ".png";
			availableIcons.add(filename);
		}
	}

	/**
	 * Connect to server.
	 *
	 * @param ip
	 *            the ip
	 * @param port
	 *            the port
	 */
	void connectToServer(String ip, int port) {
		logger.info("attempting connect to server " + ip + " : " + port);

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Client.class.getResource("/ca/tfuller/dfshortcuttutor/client/playerInit.fxml"));
			AnchorPane ap = (AnchorPane) loader.load();
			playerInitController = (PlayerInitController) loader.getController();

			socketReadWriter = new SocketSelectorReadWriter(this, ip, port);

			readWriterThread = new Runnable() {
				@Override
				public void run() {
					socketReadWriter.start();
				}
			};

			executor = Executors.newCachedThreadPool();
			executor.submit(readWriterThread);

			setContent(ap);

		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	/**
	 * Respond to game created message from server
	 *
	 * @param gameId
	 *            the game id that was created
	 * @param ownerId
	 *            the owner of the game that was created
	 * @param difficulty
	 *            the difficulty of the game that was created
	 */
	void gameCreated(int gameId, int ownerId, Difficulty difficulty) {
		Game game = new Game(gameId);
		Player owner = players.get(ownerId);
		game.setOwner(owner);
		game.getPlayers().add(owner);
		game.setDifficulty(difficulty);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				games.add(game);
			}
		});

		gameChooserController.refreshGameList();

		logger.info("game: " + game.getId() + " owner:" + game.getOwner().getId());
	}

	/**
	 * Gets the games.
	 *
	 * @return the games
	 */
	ObservableList<Game> getGames() {
		return games;
	}

	/**
	 * Look through the list of games and return the game that this player
	 * (client) owns.
	 *
	 * @return the owner of the game
	 * 
	 */
	Game getOwnerGame() {
		for (Game game : games) {
			if (game.getOwner().getId() == player.getId()) {
				return game;
			}
		}
		return null;
	}

	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	Player getPlayer() {
		return player;
	}

	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	Map<Integer, Player> getPlayers() {
		return players;
	}

	/**
	 * Respond to broadcast message that some player joined a game
	 *
	 * @param gameId
	 *            the game id that the player joined
	 * @param playerId
	 *            the player id that joined the game
	 * 
	 */
	void joinedGame(int gameId, int playerId) {
		for (Game game : games) {
			if (game.getId() == gameId) {
				Player playerjoined = players.get(playerId);
				game.getPlayers().add(playerjoined);
				gameChooserController.refreshGameList();
			}
		}
	}

	/**
	 * Player removed.
	 *
	 * @param playerId
	 *            the player id
	 */
	void playerRemoved(int playerId) {
		players.remove(playerId);

		for (Game game : games) {
			Iterator<Player> playerIt = game.getPlayers().iterator();
			while (playerIt.hasNext()) {
				Player plr = playerIt.next();
				if (plr.getId() == playerId) {
					playerIt.remove();
				}
			}
		}
		gameChooserController.refreshGameList();
	}

	/**
	 * Request create game.
	 *
	 * @param difficulty
	 *            the difficulty
	 */
	/*
	 * Send server create game request
	 */
	void requestCreateGame(Difficulty difficulty) {
		String message = CommActionClientSend.CREATEGAME.name() + " " + difficulty.name();
		socketReadWriter.send(message);
	}

	/**
	 * Send server delete game request.
	 */
	void requestDeleteGame() {
		String message = CommActionClientSend.DELETEGAME.name() + " " + getOwnerGame().getId();
		socketReadWriter.send(message);
	}

	void requestGameWinner() {
		String message = CommActionClientSend.PLAYERWIN.name() + " " + getOwnerGame().getId();
		socketReadWriter.send(message);
	}

	/**
	 * Request initialize player. Called when next clicked on playerInit screen
	 *
	 * @param string
	 *            the string
	 * @param iconFileName
	 *            the icon file name
	 */
	void requestInitializePlayer(String string, String iconFileName) {
		player.setName(string);
		player.setIconFileName(iconFileName);
		logger.info(CommActionClientSend.SETPLAYERINFO.name() + " " + player.getId() + " " + player.getName() + " "
				+ player.getIconFileName() + " into theadCommsQueue");
		socketReadWriter.send(CommActionClientSend.SETPLAYERINFO.name() + " " + player.getId() + " " + player.getName()
				+ " " + player.getIconFileName());
		socketReadWriter.send(CommActionClientSend.GETGAMELIST.name());

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Client.class.getResource("/ca/tfuller/dfshortcuttutor/client/gameChooser.fxml"));
		AnchorPane ap;
		try {
			ap = (AnchorPane) loader.load();
			gameChooserController = (GameChooserController) loader.getController();
			setContent(ap);
		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	/**
	 * Request join game.
	 *
	 * @param game
	 *            the game
	 */
	/*
	 * Send server request for this client to join game.
	 */
	void requestJoinGame(Game game) {
		String message = CommActionClientSend.JOINGAME.name() + " " + game.getId() + " " + player.getId();
		socketReadWriter.send(message);
	}

	/**
	 * Request leave game.
	 */
	void requestLeaveGame() {
		String message = CommActionClientSend.LEAVEGAME.name() + " " + getJoinedGame().getId() + " " + player.getId();
		socketReadWriter.send(message);
	}

	/**
	 * Request set game difficulty.
	 *
	 * @param difficulty
	 *            the difficulty
	 * 
	 *            Called by GameChooserController. Relay request to set game
	 *            difficulty to server.
	 */
	void requestSetGameDifficulty(Difficulty difficulty) {
		String message = CommActionClientSend.SETDIFFICULTY.name() + " " + getOwnerGame().getId() + " "
				+ difficulty.name();
		socketReadWriter.send(message);
	}

	/**
	 * Request player advance.
	 */
	void requestPlayerAdvance() {
		String message = CommActionClientSend.PLAYERADVANCE.name() + " " + runningGame.getId() + " " + player.getId();
		socketReadWriter.send(message);
	}

	/**
	 * Request start game.
	 */
	/*
	 * Start the game that this player (client) owns
	 * Send the start game request to server
	 */
	void requestStartGame() {
		Game game = getOwnerGame();
		String message = CommActionClientSend.STARTGAME.name() + " " + game.getId();
		socketReadWriter.send(message);
	}

	/**
	 * Sets the connect to server controller.
	 *
	 * @param connectToServerController
	 *            the new connect to server controller
	 */
	void setConnectToServerController(ConnectToServerController connectToServerController) {
		this.connectToServerController = connectToServerController;
	}

	/**
	 * Sets the content.
	 *
	 * @param ap
	 *            the new content
	 */
	void setContent(AnchorPane ap) {
		anchorPaneCenter.getChildren().clear();
		anchorPaneCenter.getChildren().add(ap);
		AnchorPane.setTopAnchor(ap, 0.0);
		AnchorPane.setLeftAnchor(ap, 0.0);
		AnchorPane.setBottomAnchor(ap, 0.0);
		AnchorPane.setRightAnchor(ap, 0.0);
	}

	/**
	 * Sets the game difficulty.
	 *
	 * @param difficultyOrdinal
	 *            the new game difficulty
	 */
	void setGameDifficulty(int gameId, Difficulty difficulty) {
		for (Game game : games) {
			if (game.getId() == gameId) {
				game.setDifficulty(difficulty);
				logger.info("game difficulty set to " + difficulty.name() + " ordinal is " + difficulty);
			}
		}
	}

	/**
	 * Sets the games.
	 *
	 * @param games
	 *            the new games
	 */
	void setGames(ObservableList<Game> games) {
		this.games = games;
	}

	/**
	 * Sets the player id.
	 *
	 * @param playerId
	 *            the new player id
	 */
	void setPlayerId(int playerId) {
		player.setId(playerId);
		player.setClientChannel(clientChannel);

		players.put(playerId, player);

		logger.info("player id set to " + player.getId());
	}

	/**
	 * Sets the player info.
	 *
	 * @param playerId
	 *            the player id
	 * @param playerName
	 *            the player name
	 * @param iconFileName
	 *            the icon file name
	 */
	void setPlayerInfo(int playerId, String playerName, String iconFileName) {
		players.put(playerId, new Player(playerId, playerName, iconFileName));
		for (Button but : playerInitController.getIconButtonList()) {
			if (((String) but.getUserData()).equals(iconFileName)) {
				but.setDisable(true);
			}
		}
	}

	/**
	 * Shut down.
	 */
	void shutdown() {
		logger.info("shutdown executing");

		try {
			executor.shutdown();
			socketReadWriter.shutdown();

			// Wait for the thread to complete or timeout after a specified time
			if (!executor.awaitTermination(5, TimeUnit.SECONDS)) {
				System.out.println("Thread didn't terminate within the specified time.");
				// Forcefully shutdown the ExecutorService if needed
				executor.shutdownNow();
			}

		} catch (InterruptedException e) {
			ErrorMessage.display(e);
		}
	}

	/**
	 * Start game.
	 *
	 * @param gameId
	 *            the game id
	 */
	void startGame(int gameId) {
		for (Game game : games) {
			if (game.getId() == gameId) {
				runningGame = game;
				logger.info("setting running game to id = " + runningGame.getId());
				runningGame.setStatus(GameStatus.RUNNING);
				break;
			}
		}

		logger.info("displaying game fxml");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Client.class.getResource("/ca/tfuller/dfshortcuttutor/client/game.fxml"));
		AnchorPane ap;
		try {
			ap = (AnchorPane) loader.load();
			gameController = (GameController) loader.getController();
			gameController.setPrimaryStage(primaryStage);

			GameController.setClientController(this);
			logger.info("madeit 6");
			setContent(ap);
			logger.info("madeit 7");
			// find the game that this client owns

			gameController.startGame(runningGame);
		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	/**
	 * Player advanced.
	 *
	 * @param playerId
	 *            the player id
	 */
	void playerAdvanced(int playerId) {
		gameController.playerAdvanced(playerId);
	}

	/**
	 * Gets the running game.
	 *
	 * @return the running game
	 */
	Game getRunningGame() {
		return runningGame;
	}

	/**
	 * Delete game.
	 *
	 * @param gameId
	 *            the game id
	 */
	void deleteGame(int gameId) {
		for (Game game : games) {
			if (game.getId() == gameId) {
				Game gameToRemove = game;
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						games.remove(gameToRemove);
					}
				});
				break;
			}
		}
		gameChooserController.refreshGameList();
	}

	/**
	 * Left game.
	 *
	 * @param gameId
	 *            the game id
	 * @param playerId
	 *            the player id
	 */
	void leftGame(int gameId, int playerId) {
		for (Game game : games) {
			if (game.getId() == gameId) {
				Player playerjoined = players.get(playerId);
				game.getPlayers().remove(playerjoined);
				gameChooserController.refreshGameList();
			}
		}
	}

	/**
	 * Return true if a player is a member of any game.
	 *
	 * @return true if user is a member of any game
	 */
	public boolean hasJoinedGame() {
		Game joinedGame = getJoinedGame();
		if (joinedGame == null) {
			return false;
		}
		return true;
	}

	/**
	 * Search through the list of games. Return the game that the user is a
	 * member of. Return null if the user has not joined a game.
	 *
	 * @return the joined game
	 */
	public Game getJoinedGame() {
		Game joinedGame = null;
		for (Game game : games) {
			for (Player plr : game.getPlayers()) {
				if (plr.getId() == player.getId()) {
					joinedGame = game;
					break;
				}
			}
		}
		return joinedGame;
	}

	void addPlayer(int playerId, String playerName, String iconFileName) {
		Player plr = new Player(playerId, playerName, iconFileName);
		logger.info("adding player " + player.getName());
		players.put(playerId, plr);
	}

	void refreshPlayerIcons() {
		playerInitController.refreshPlayerIcons();
	}

	/**
	 * Display received.
	 *
	 * @param client
	 *            the client
	 * @param message
	 *            the message
	 */
	void displayReceived(String message) {
		textAreaReceive.appendText(System.lineSeparator() + message);
	}

	/**
	 * Display transmit.
	 *
	 * @param client
	 *            the client
	 * @param message
	 *            the message
	 */
	void displayTransmit(String message) {
		textAreaTransmit.appendText(System.lineSeparator() + message);
	}

	/**
	 * @param gameId
	 * @param playerId
	 */
	public void requestPlayerWin(int gameId, int playerId) {
		String message = CommActionClientSend.PLAYERWIN.name() + " " + gameId + " " + playerId;
		socketReadWriter.send(message);
	}

	/**
	 * @param gameId
	 * @param playerId
	 */
	public void playerWon(int gameId, int playerId) {
		logger.info("player won");
		gameController.playerWon(gameId, playerId);
	}

}
