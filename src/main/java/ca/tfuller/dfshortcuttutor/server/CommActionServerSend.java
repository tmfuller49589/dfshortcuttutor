package ca.tfuller.dfshortcuttutor.server;

/**
 * These are messages that the server sends and are processed by the client
 */
public enum CommActionServerSend {
	//@formatter:off
	CONNECTED(1), 
	SETPLAYERINFO(3), 
	GAMECREATED(3), 		// gameid ownerId difficulty
	GAMEDELETED(1), 		// gameid
	TERMINATE(0), 
	PLAYERLIST(0), 
	GAMELIST(0), 
	JOINEDGAME(2),			// gameid playerid
	LEFTGAME(2),			// gameid playerid
	STARTEDGAME(1), 		// gameId
	DIFFICULTYSET(2), 		// gameId difficulty
	PLAYERREMOVED(1), 		// playerid  occurs at client shutdown
	PLAYERADVANCED(1), 
	PING(0), 
	SHUTDOWN(0), 
	PLAYERWON(2);			// gameid playerid
	//@formatter:on

	int numberOfParameters;
	String[] parameters;

	CommActionServerSend(int i) {
		numberOfParameters = i;
		parameters = new String[numberOfParameters];
	}

	public static CommActionServerSend parse(String string) {
		if (string.isEmpty()) {
			return null;
		}
		for (CommActionServerSend ca : CommActionServerSend.values()) {
			if (ca.name().equals(string)) {
				return ca;
			}
		}
		return null;
	}

	public static CommActionServerSend parseWithParameters(String string) {
		String[] tokens = string.split(" ");
		CommActionServerSend cas = parse(tokens[0]);
		for (int i = 0; i < cas.numberOfParameters; ++i) {
			cas.parameters[i] = tokens[i + 1];
		}
		return cas;
	}

	public String getCommString() {
		String s = name();
		for (int i = 0; i < numberOfParameters; ++i) {
			s += " " + parameters[i];
		}
		return s;
	}

	public String getParameter(int i) {
		return parameters[i];
	}
}
