package ca.tfuller.dfshortcuttutor.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.gui.Difficulty;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * The Class ServerController.
 *
 * @author tfuller
 */
public class ServerController {

	/** The Constant logger. */
	protected static final Logger logger = LogManager.getLogger(ServerController.class);

	/** The Constant EOM. */
	public static final String EOM = "EOM";

	/** The Constant EOL. */
	public static final String EOL = "EOL";

	/** The Constant lastIpFile. */
	public static final String lastIpFile = "lastIpUsed.txt";

	/** The Constant BUFFERSIZE. */
	public static final int BUFFERSIZE = 1024;

	/** The players. */
	private Map<Integer, Player> players = new HashMap<Integer, Player>();

	/** The games map. */
	private Map<Integer, Game> gamesMap = new HashMap<Integer, Game>();

	/** The button start. */
	@FXML
	Button buttonStart;

	/** The button stop. */
	@FXML
	Button buttonStop;

	/** The text field port. */
	@FXML
	TextField textFieldPort;

	/** The label server status. */
	@FXML
	Label labelServerStatus;

	/** The list view games. */
	@FXML
	ListView<Game> listViewGames;

	/** The games list. */
	private ObservableList<Game> gamesList = FXCollections.observableArrayList();

	/** The text area transmit. */
	@FXML
	TextArea textAreaTransmit;

	/** The text area receive. */
	@FXML
	TextArea textAreaReceive;

	/** The button ping. */
	@FXML
	Button buttonPing;

	/** The combobox ip. */
	@FXML
	ComboBox<String> comboboxIp;

	/** The socket manager. */
	private SocketManager socketManager;

	/** The executor. */
	private ExecutorService executor;

	/**
	 * Sets the primary stage.
	 *
	 * @param primaryStage
	 *            the new primary stage
	 */
	void setPrimaryStage(Stage primaryStage) {

	}

	/**
	 * Initialize.
	 */
	@FXML
	public void initialize() {
		try {
			Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
			for (NetworkInterface netint : Collections.list(nis)) {
				Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
				displayInterfaceInformation(netint);
				for (InetAddress inetAddress : Collections.list(inetAddresses)) {
					if (inetAddress instanceof Inet4Address && !inetAddress.isLoopbackAddress()) {
						comboboxIp.getItems().add(inetAddress.toString().replace("/", ""));
					}
				}
			}
		} catch (SocketException e) {
			ErrorMessage.display(e);
		}

		File file = new File(lastIpFile);
		if (file.exists()) {
			Path path = file.toPath();
			try {
				List<String> lines = Files.readAllLines(path);
				String ipAddress = lines.get(0);
				logger.info("read " + ipAddress + " from " + lastIpFile);
				comboboxIp.getSelectionModel().select(ipAddress);
			} catch (IOException e) {
				ErrorMessage.display(e);
			}
		}

		listViewGames.setCellFactory(param -> new ListCell<Game>() {
			@Override
			protected void updateItem(Game game, boolean empty) {
				super.updateItem(game, empty);

				if (empty || game == null || game.getName() == null) {
					setText(null);
				} else {
					HBox hbox = new HBox();
					String filename = game.getOwner().getIconFileName();
					logger.info("icon file name is " + filename);
					logger.info("owner name is " + game.getOwner().getName());
					setText(game.getName() + " " + game.getOwner().getName());

					for (Player player : game.getPlayers()) {
						logger.info("player icon file is " + player.getIconFileName());
						Image image = new Image(
								ServerController.class.getClassLoader().getResourceAsStream(player.getIconFileName()));
						ImageView imView = new ImageView(image);
						imView.setFitHeight(70);
						imView.setFitWidth(70);
						imView.setPreserveRatio(true);
						hbox.getChildren().add(imView);
					}

					setGraphic(hbox);
				}
			}
		});

		listViewGames.setItems(gamesList);
		labelServerStatus.setText("server stopped");
		buttonPing.setDisable(true);
	}

	/**
	 * Display interface information.
	 *
	 * @param netint
	 *            the netint
	 * @throws SocketException
	 *             the socket exception
	 */
	static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
		System.out.printf("Display name: %s\n", netint.getDisplayName());
		System.out.printf("Name: %s\n", netint.getName());
		Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
		for (InetAddress inetAddress : Collections.list(inetAddresses)) {
			System.out.printf("InetAddress: %s\n", inetAddress);
		}
		System.out.printf("\n");
	}

	/**
	 * Start clicked.
	 */
	@FXML
	private void startClicked() {

		logger.info(
				"start server " + comboboxIp.getSelectionModel().getSelectedItem() + " : " + textFieldPort.getText());
		socketManager = new SocketManager(this, comboboxIp.getSelectionModel().getSelectedItem(),
				Integer.parseInt(textFieldPort.getText()));

		Runnable r = new Runnable() {
			@Override
			public void run() {
				socketManager.start();
			}
		};

		executor = Executors.newCachedThreadPool();
		executor.submit(r);

		File file = new File(lastIpFile);
		try {
			PrintWriter pw = new PrintWriter(file);
			pw.print(comboboxIp.getSelectionModel().getSelectedItem());
			pw.close();
		} catch (FileNotFoundException e) {
			ErrorMessage.display(e);
		}

		labelServerStatus.setText("server accepting connections");
	}

	/**
	 * Server status callback.
	 *
	 * @param running
	 *            the running
	 */
	void serverStatusCallback(boolean running) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				buttonStart.setDisable(running);
				if (running) {
					labelServerStatus.setText("server running");
					buttonPing.setDisable(false);
				} else {
					labelServerStatus.setText("server stopped");
					buttonPing.setDisable(true);
				}
			}
		});
	}

	/**
	 * Stop clicked.
	 */
	@FXML
	private void stopClicked() {
		shutdown();
	}

	/**
	 * Ping clicked.
	 */
	@FXML
	void pingClicked() {
		socketManager.pingClients();
	}

	/**
	 * Creates the player.
	 *
	 * @param clientSocketChannel
	 *            the client socket channel
	 * @return the playerId, which is a hashCode of the clientSocket channel, a
	 *         large integer value
	 */
	int createPlayer(SocketChannel clientSocketChannel) {
		int playerId = clientSocketChannel.hashCode();
		Player player = new Player();
		player.setId(playerId);
		player.setClientChannel(clientSocketChannel);
		players.put(playerId, player);

		return playerId;

	}

	/**
	 * Display received.
	 *
	 * @param client
	 *            the client
	 * @param message
	 *            the message
	 */
	void displayReceived(String message) {
		textAreaReceive.appendText(message);
	}

	/**
	 * Display transmit.
	 *
	 * @param client
	 *            the client
	 * @param message
	 *            the message
	 */
	void displayTransmit(String message) {
		textAreaTransmit.appendText(message);
	}

	/**
	 * Creates the game.
	 *
	 * @param clientChannel
	 *            the client channel
	 * @return the game
	 */
	Game createGame(SocketChannel clientChannel) {
		Game game = new Game();
		gamesMap.put(game.getId(), game);
		gamesList.add(game);
		Player player = players.get(clientChannel.hashCode());
		game.setOwner(player);
		game.getPlayers().add(player);
		return game;
	}

	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	Map<Integer, Player> getPlayers() {
		return players;
	}

	/**
	 * Gets the games.
	 *
	 * @return the games
	 */
	Map<Integer, Game> getGames() {
		return gamesMap;
	}

	/**
	 * Player joined.
	 *
	 * @param gameId
	 *            the game id
	 * @param playerId
	 *            the player id
	 */
	void playerJoined(int gameId, Integer playerId) {
		Game game = gamesMap.get(gameId);
		Player player = players.get(playerId);
		game.getPlayers().add(player);
		refreshListViewGames();
	}

	/**
	 * Player left.
	 *
	 * @param gameId
	 *            the game id
	 * @param playerId
	 *            the player id
	 */
	void playerLeft(int gameId, Integer playerId) {
		Game game = gamesMap.get(gameId);
		Player player = players.get(playerId);
		game.getPlayers().remove(player);
		refreshListViewGames();
	}

	/**
	 * Removes the player.
	 *
	 * @param playerId
	 *            the player id
	 * @return the game
	 */
	Game removePlayer(Integer playerId) {
		Player player = players.get(playerId);

		// remove player from games
		Game gamePlayerOwned = null;
		for (Entry<Integer, Game> entry : gamesMap.entrySet()) {
			Iterator<Player> playerIt = entry.getValue().getPlayers().iterator();
			if (entry.getValue().getOwner().getId() == playerId) {
				gamePlayerOwned = entry.getValue();
			}
			while (playerIt.hasNext()) {
				Player playerTmp = playerIt.next();
				if (playerTmp.getId() == player.getId()) {
					playerIt.remove();
				}
			}
		}

		// remove game that player owned
		if (gamePlayerOwned != null) {
			gamesMap.remove(gamePlayerOwned.getId());

			final Game gameToRemove = gamePlayerOwned;
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					gamesList.remove(gameToRemove);
					players.remove(playerId);
					listViewGames.refresh();
				}
			});
		}
		return gamePlayerOwned;
	}

	/**
	 * Refresh list view games.
	 */
	void refreshListViewGames() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				listViewGames.refresh();
			}
		});
	}

	/**
	 * Shutdown.
	 */
	void shutdown() {
		logger.info("shutdown executing");

		if (executor == null) {
			return;
		}

		try {
			socketManager.shutdown();
			executor.shutdown();

			// Wait for the thread to complete or timeout after a specified time
			if (!executor.awaitTermination(5, TimeUnit.SECONDS)) {
				System.out.println("Thread didn't terminate within the specified time.");
				// Forcefully shutdown the ExecutorService if needed
				executor.shutdownNow();
			}
		} catch (InterruptedException e) {
			ErrorMessage.display(e);
		}
	}

	public void deleteGame(int gameid) {
		Game game = gamesMap.remove(gameid);
		gamesList.remove(game);
		refreshListViewGames();
	}

	public void playerWon(int gameId, Integer playerId) {
		//TODO: need to do something here?
		logger.info("TODO: need to do something here?");

	}

	/**
	 * @param gameId
	 * @param difficulty
	 */
	public void setDifficulty(int gameId, Difficulty difficulty) {
		Game game = gamesMap.get(gameId);
		game.setDifficulty(difficulty);
	}
}
