package ca.tfuller.dfshortcuttutor.server;

import java.io.IOException;
import java.lang.Runtime.Version;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Server extends Application {

	protected static final Logger logger = LogManager.getLogger(Server.class);

	private Stage primaryStage;
	private AnchorPane rootLayout;
	private ServerController serverController;

	private static Scene scene;

	@Override
	public void start(Stage priStage) {
		try {

			Version version = java.lang.Runtime.version();
			System.out.println("Java Version = " + version);

			this.primaryStage = priStage;
			this.primaryStage.setTitle("Eclipse Shortcut Tutor: Server");

			initRootLayout();

		} catch (Exception e) {
			ErrorMessage.display(e);
		}
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Server.class.getResource("/ca/tfuller/dfshortcuttutor/server/server.fxml"));
			rootLayout = (AnchorPane) loader.load();

			serverController = (ServerController) loader.getController();

			// Show the scene containing the root layout.
			scene = new Scene(rootLayout);

			primaryStage.setScene(scene);
			primaryStage.show();
			serverController.setPrimaryStage(primaryStage);

		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	@Override
	public void stop() {
		serverController.shutdown();
	}

	public static void main(String[] args) {

		launch(args);
	}

}
