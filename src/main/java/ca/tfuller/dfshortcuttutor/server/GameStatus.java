package ca.tfuller.dfshortcuttutor.server;

public enum GameStatus {
	CREATED, RUNNING, ENDED;
}
