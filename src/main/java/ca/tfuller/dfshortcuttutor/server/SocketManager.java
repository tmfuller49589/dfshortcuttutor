package ca.tfuller.dfshortcuttutor.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.gui.Difficulty;
import javafx.application.Platform;

public class SocketManager {

	private ServerSocketChannel serverChannel;
	private String host;
	private int port;
	private ServerController serverController;
	private Selector selector;
	private boolean shutdown = false;

	protected static final Logger logger = LogManager.getLogger(SocketManager.class);

	public SocketManager(ServerController serverController, String host, int port) {
		this.host = host;
		this.port = port;
		this.serverController = serverController;
	}

	public void start() {
		try {
			selector = Selector.open();
			serverChannel = ServerSocketChannel.open();
			InetSocketAddress hostAddress = new InetSocketAddress(host, port);
			serverChannel.bind(hostAddress);
			serverChannel.configureBlocking(false);
			serverChannel.register(selector, SelectionKey.OP_ACCEPT);

			logger.info("listening for clients");
			ByteBuffer buffer = ByteBuffer.allocate(256);
			serverController.serverStatusCallback(true);
			while (!shutdown) {
				selector.select(1000);
				Set<SelectionKey> selectedKeys = selector.selectedKeys();
				Iterator<SelectionKey> iter = selectedKeys.iterator();
				while (iter.hasNext()) {
					SelectionKey key = iter.next();
					iter.remove();

					if (key.isAcceptable()) {
						SocketChannel client = serverChannel.accept();
						client.configureBlocking(false);
						client.register(selector, SelectionKey.OP_READ);
					}

					if (key.isReadable()) {
						processClientMessage(buffer, key);
					}

				}
			}

			serverChannel.close();
			selector.close();

			serverController.serverStatusCallback(false);

		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	private void processClientMessage(ByteBuffer buffer, SelectionKey key) {
		SocketChannel client = (SocketChannel) key.channel();
		int r;
		try {
			buffer.clear();
			r = client.read(buffer);
			String message = new String(buffer.array()).trim().substring(0, r);
			logger.info("received client message -->" + message + "<--");

			String remoteAddress = "";
			try {
				remoteAddress = client.getRemoteAddress().toString();
			} catch (IOException e) {
			}

			final String msg = System.lineSeparator() + remoteAddress + " " + message;

			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					serverController.displayReceived(msg);
				}
			});
			if (r == -1) {
				client.close();
				logger.info("Not accepting client messages anymore");
			}

			CommActionClientSend commAction = CommActionClientSend.parseWithParameters(message);
			String response = "";
			logger.info("commAction is " + commAction);
			switch (commAction) {
			case CONNECT:
				response = CommActionServerSend.CONNECTED.name();
				response += " ";
				response += client.hashCode() + " ";
				response += ServerController.EOM;
				serverController.createPlayer(client);
				send(client, response);
				break;

			case CREATEGAME:
				Game game = serverController.createGame(client);
				response = CommActionServerSend.GAMECREATED.name() + " ";
				response += Integer.toString(game.getId()) + " ";
				response += Integer.toString(game.getOwner().getId()) + " ";
				response += commAction.getParameter(0) + " ";
				response += ServerController.EOM;
				broadcast(response);

				break;
			case SETPLAYERINFO:
				response = CommActionServerSend.SETPLAYERINFO.name();
				Integer playerId = Integer.parseInt(commAction.parameters[0]);
				String playerName = commAction.parameters[1];
				String iconFileName = commAction.parameters[2];
				response += " ";
				response += playerId;
				response += " ";
				response += playerName;
				response += " ";
				response += iconFileName;
				response += " ";
				response += ServerController.EOM;
				Player player = serverController.getPlayers().get(playerId);
				if (player == null) {
					player = new Player(playerId, playerName, iconFileName);
					serverController.getPlayers().put(playerId, player);
				} else {
					player.setName(playerName);
					player.setIconFileName(iconFileName);
				}
				logger.info("response is " + response);
				broadcast(response);
				break;
			case GETGAMELIST:
				response = CommActionServerSend.GAMELIST.name();
				response += " ";

				for (Entry<Integer, Game> entry : serverController.getGames().entrySet()) {
					response += entry.getValue().getId() + " ";
					response += entry.getValue().getName() + " ";
					response += entry.getValue().getOwner().getId() + " ";
					response += entry.getValue().getDifficulty().name() + " ";

					for (Player playerTmp : entry.getValue().getPlayers()) {
						response += playerTmp.getId() + " ";
					}
					response += ServerController.EOL;
				}
				response += " " + ServerController.EOM;
				send(client, response);
				break;
			case GETPLAYERLIST:
				response = CommActionServerSend.PLAYERLIST.name();
				response += " ";

				for (Entry<Integer, Player> entry : serverController.getPlayers().entrySet()) {
					logger.info("adding player to response list " + entry.getValue().getId() + " "
							+ entry.getValue().getName() + " " + entry.getValue().getIconFileName());
					response += entry.getValue().getId() + " ";
					response += entry.getValue().getName() + " ";
					response += entry.getValue().getIconFileName() + " ";
				}
				response += ServerController.EOM;
				send(client, response);
				break;
			case JOINGAME:
				// JOINGAME gameid playerid
				response = CommActionServerSend.JOINEDGAME.name() + " ";
				response += commAction.getParameter(0) + " ";
				response += commAction.getParameter(1) + " ";
				response += ServerController.EOM;
				int gameId = Integer.parseInt(commAction.getParameter(0));
				playerId = Integer.parseInt(commAction.getParameter(1));
				serverController.playerJoined(gameId, playerId);
				broadcast(response);
				break;
			case LEAVEGAME:
				// LEAVEGAME gameid playerid
				response = CommActionServerSend.LEFTGAME.name() + " ";
				response += commAction.getParameter(0) + " ";
				response += commAction.getParameter(1) + " ";
				response += ServerController.EOM;
				gameId = Integer.parseInt(commAction.getParameter(0));
				playerId = Integer.parseInt(commAction.getParameter(1));
				serverController.playerLeft(gameId, playerId);
				broadcast(response);
				break;
			case STARTGAME:
				// STARTGAME gameid
				response = CommActionServerSend.STARTEDGAME.name() + " ";
				gameId = Integer.parseInt(commAction.getParameter(0));
				response += gameId + " ";
				response += ServerController.EOM;
				broadcastGame(response, gameId);
				break;
			case CLIENTSHUTDOWN:
				//remove player & broadcast
				playerId = Integer.parseInt(commAction.parameters[0]);
				response = CommActionServerSend.PLAYERREMOVED.name() + " ";
				response += Integer.toString(playerId) + " ";
				response += ServerController.EOM;
				Game gameToRemove = serverController.removePlayer(playerId);
				broadcast(response);
				client.close();
				key.cancel();

				if (gameToRemove != null) {
					response = CommActionServerSend.GAMEDELETED.name() + " ";
					response += gameToRemove.getId() + " ";
					response += ServerController.EOM;
					broadcast(response);
				}
				break;
			case SETDIFFICULTY:
				response += CommActionServerSend.DIFFICULTYSET.name() + " ";
				gameId = Integer.parseInt(commAction.getParameter(0));
				Difficulty difficulty = Difficulty.parse(commAction.getParameter(1));

				response += gameId + " ";
				response += difficulty.name() + " ";
				response += ServerController.EOM;
				serverController.setDifficulty(gameId, difficulty);
				broadcast(response);
				break;
			case PLAYERADVANCE:
				response += CommActionServerSend.PLAYERADVANCED.name() + " ";
				gameId = Integer.parseInt(commAction.getParameter(0));
				playerId = Integer.parseInt(commAction.getParameter(1));
				response += playerId + " ";
				response += ServerController.EOM;
				broadcastGame(response, gameId);
				break;
			case PONG:
				logger.info("PONG response from client " + commAction.getParameter(0));
				break;
			case DELETEGAME:
				gameId = Integer.parseInt(commAction.getParameter(0));
				serverController.deleteGame(gameId);
				response = CommActionServerSend.GAMEDELETED + " " + gameId + " ";
				response += ServerController.EOM;
				broadcast(response);
				break;
			case PLAYERWIN:
				gameId = Integer.parseInt(commAction.getParameter(0));
				playerId = Integer.parseInt(commAction.getParameter(1));
				serverController.playerWon(gameId, playerId);
				response = CommActionServerSend.PLAYERWON + " " + gameId + " " + playerId + " ";
				response += ServerController.EOM;
				broadcast(response);
				break;
			}

		} catch (IOException e) {
			ErrorMessage.display(e);
			logger.info("IOException");
		}
	}

	/*
	 * send to client players that have joined this game
	 */
	private void broadcastGame(String message, int gameId) {
		Game game = serverController.getGames().get(gameId);
		logger.info("broadcast start game " + game.getName() + " to " + game.getPlayers().size() + " players");

		for (Player player : game.getPlayers()) {
			logger.info("sending to " + player.getName());
			send(player.getClientChannel(), message);
		}
	}

	/*
	 * send to all clients
	 */
	private void broadcast(String message) {
		for (SelectionKey clientKey : selector.keys()) {
			if (clientKey.isValid() && clientKey.channel() instanceof SocketChannel) {
				SocketChannel client = (SocketChannel) clientKey.channel();
				send(client, message);
			}
		}
	}

	void pingClients() {
		String message = CommActionServerSend.PING + " " + ServerController.EOM;
		broadcast(message);
	}

	private void send(SocketChannel client, String message) {
		logger.info("client channel is " + client);
		String remoteAddress = "";
		try {
			remoteAddress = client.getRemoteAddress().toString();
		} catch (IOException e) {
		}

		final String msg = System.lineSeparator() + remoteAddress + " " + message;

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				serverController.displayTransmit(msg);
			}
		});

		ByteBuffer buffer = ByteBuffer.allocate(256);
		buffer.clear();
		buffer.put(message.getBytes());
		buffer.flip();

		try {
			logger.info("sending to client -->" + message + "<--");
			while (buffer.hasRemaining()) {
				int x = client.write(buffer);
				logger.info("bytes sent to client: " + x);
			}
			buffer.clear();
		} catch (IOException e) {
			ErrorMessage.display(e);
			logger.info("IOException");
		}
	}

	public void shutdown() {
		shutdown = true;
	}
}
