package ca.tfuller.dfshortcuttutor.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.dfshortcuttutor.gui.DFShortcutTutor;
import ca.tfuller.dfshortcuttutor.gui.Difficulty;
import ca.tfuller.dfshortcuttutor.gui.ShortcutInfo;

/**
 * 
 */
public class Game {
	protected static final Logger logger = LogManager.getLogger(Game.class);

	private String name;
	private List<Player> players = new ArrayList<Player>();
	private Player owner;

	private LocalTime startTime;
	private LocalTime endTime;
	private GameStatus status;
	private int numberOfQuestions = 5;
	private Difficulty difficulty = Difficulty.PRACTICE;

	private static int idCounter = 0;
	private int id;
	private static final List<ShortcutInfo> shortcuts = new ArrayList<ShortcutInfo>();

	{
		loadQuestions();
	}

	private static final String shortcutsFilename = "shortcuts.csv";

	/* Clients call this constructor after
	* getting game id from server.
	*/
	public Game(int id) {
		this.id = id;
		name = "game:" + id;
		status = GameStatus.CREATED;
	}

	public Game(int id, Player owner, String name, Difficulty difficulty) {
		this.id = id;
		this.owner = owner;
		this.name = name;
		status = GameStatus.CREATED;
		this.difficulty = difficulty;
		logger.info(toString());
	}

	@Override
	public String toString() {
		String s = "game: " + name + " " + id + " " + difficulty.name();
		return s;
	}

	/* Only server is supposed to call this constructor
	 * that way only server will update game id
	 */
	protected Game() {
		status = GameStatus.CREATED;

		// create a unique id for each game
		id = idCounter;
		name = "game:" + id;
		++idCounter;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public GameStatus getStatus() {
		return status;
	}

	public void setStatus(GameStatus status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void startGame() {
		this.startTime = LocalTime.now();
		status = GameStatus.RUNNING;
	}

	public void endGame() {
		this.endTime = LocalTime.now();
		status = GameStatus.ENDED;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}

	public void setNumberOfQuestions(int numberOfQuestions) {
		logger.info("setting number of questions to " + numberOfQuestions);
		this.numberOfQuestions = numberOfQuestions;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
		logger.info("game " + getId() + " difficulty = " + difficulty.name());
	}

	private static void loadQuestions() {
		InputStream is = DFShortcutTutor.class.getClassLoader().getResourceAsStream(shortcutsFilename);

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
			while (reader.ready()) {
				String line = reader.readLine();
				String[] tokens = line.split(",");
				if (tokens.length == 6) {
					ShortcutInfo sci = new ShortcutInfo(tokens);
					shortcuts.add(sci);
				}
			}

		} catch (IOException e) {
			ErrorMessage.display(e);
		}
	}

	public List<ShortcutInfo> getShortcuts() {
		return shortcuts;
	}

	private void printShortcuts() {
		logger.info("printing shortcuts");
		for (ShortcutInfo s : shortcuts) {
			logger.info(s);
		}
	}

	public boolean hasWinner() {
		if (getWinner() == null) {
			return false;
		}
		return true;
	}

	/**
	 * @return the winning Player if someone has won, otherwise return null
	 */
	public Player getWinner() {
		for (Player player : getPlayers()) {
			logger.info("player " + player.getName() + " score: " + player.getNumCorrect() + " / "
					+ getNumberOfQuestions());
			if (player.getNumCorrect() == getNumberOfQuestions()) {
				return player;
			}
		}
		return null;
	}

	/**
	 * @param playerId
	 */
	public void advancePlayer(int playerId) {
		for (Player player : getPlayers()) {
			if (player.getId() == playerId) {
				player.incrementCorrect();
			}
		}
	}
}
