package ca.tfuller.dfshortcuttutor.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * These are communication actions sent by the client to the server
 */
public enum CommActionClientSend {

	//@formatter:off
	CONNECT(0), 			//
	SETPLAYERINFO(3), 		//
	CREATEGAME(1), 			// difficulty
	DELETEGAME(1),			// gameid
	GETPLAYERLIST(0), 		//
	GETGAMELIST(0), 		//
	JOINGAME(2), 			// gameid playerid
	LEAVEGAME(2), 			// gameid playerid
	STARTGAME(1), 			// gameid 
	CLIENTSHUTDOWN(1), 		// client id
	SETDIFFICULTY(2), 		// gameid difficulty
	PONG(1), 				// playerid  respond to server ping
	PLAYERADVANCE(2), 		// gameid playerid    player got question correct, advance
	PLAYERWIN(2);			// gameid playerid  player won the game
	// @formatter:on

	protected static final Logger logger = LogManager.getLogger(CommActionClientSend.class);

	int numberOfParameters;
	String[] parameters;

	CommActionClientSend(int i) {
		numberOfParameters = i;
		parameters = new String[numberOfParameters];
	}

	public static CommActionClientSend parse(String string) {
		if (string.isEmpty()) {
			return null;
		}
		for (CommActionClientSend ca : CommActionClientSend.values()) {
			if (ca.name().equals(string)) {
				return ca;
			}
		}
		return null;
	}

	public static CommActionClientSend parseWithParameters(String string) {
		//TODO check that number of parameters matches numberOfParameters
		String[] tokens = string.split(" ");
		logger.info("-->" + string + "<--");
		CommActionClientSend ca = parse(tokens[0]);
		logger.info("-->" + tokens[0] + "<--");
		for (int i = 0; i < ca.numberOfParameters; ++i) {
			logger.info("-->" + tokens[i + 1] + "<--");
			ca.parameters[i] = tokens[i + 1];
		}
		logger.info("returning  " + ca);
		return ca;
	}

	public String getCommString() {
		String s = name();
		for (int i = 0; i < numberOfParameters; ++i) {
			s += " " + parameters[i];
		}
		return s;
	}

	public String getParameter(int i) {
		return parameters[i];
	}

}
