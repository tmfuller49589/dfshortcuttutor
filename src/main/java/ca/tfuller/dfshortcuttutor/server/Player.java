package ca.tfuller.dfshortcuttutor.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.channels.SocketChannel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.image.ImageView;

public class Player implements Serializable {

	private transient static final long serialVersionUID = 8132714402388209077L;
	protected transient static final Logger logger = LogManager.getLogger(Player.class);

	private Integer id = -1;
	private String name;
	private String iconFileName;

	private transient ImageView imageView;
	private transient int numCorrect;
	private transient SocketChannel clientChannel;

	public Player() {
	}

	public Player(int id, String name, String iconFileName) {
		this.id = id;
		this.name = name;
		this.iconFileName = iconFileName;
	}

	public SocketChannel getClientChannel() {
		return clientChannel;
	}

	public void setClientChannel(SocketChannel clientChannel) {
		this.clientChannel = clientChannel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		this.name = name.replace(" ", "_");
	}

	public ImageView getImageView() {
		return imageView;
	}

	public void setImageView(ImageView imageView) {
		this.imageView = imageView;
	}

	public int getNumCorrect() {
		return numCorrect;
	}

	public void setNumCorrect(int numCorrect) {
		this.numCorrect = numCorrect;
	}

	public void incrementCorrect() {
		++numCorrect;
		logger.info("player " + name + " score is " + numCorrect);
	}

	private void writeObject(ObjectOutputStream oos) throws IOException {
		oos.defaultWriteObject();
		//oos.writeObject(imageV);
	}

	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
		ois.defaultReadObject();
		//Integer houseNumber = (Integer) ois.readObject();
		//Address a = new Address();
		//a.setHouseNumber(houseNumber);
		//this.setAddress(a);
	}

	public String getIconFileName() {
		return iconFileName;
	}

	public void setIconFileName(String iconFileName) {
		logger.info("setting icon file name to " + iconFileName);
		this.iconFileName = iconFileName;
	}

}
