package ca.tfuller.dfshortcuttutor.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ErrorMessage {
	protected static final Logger logger = LogManager.getLogger(ErrorMessage.class);

	public static void display(Exception e) {

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				e.printStackTrace();
				try (StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)) {
					e.printStackTrace(pw);

					// filter the stack trace, include only lines from code that fuller wrote
					String emsg = sw.toString();
					String[] lines = emsg.split(System.lineSeparator());
					String dmsg = "";
					for (String s : lines) {
						if (s.contains("fuller")) {
							dmsg += s;
						}
					}
					Alert a = new Alert(AlertType.ERROR);
					a.setTitle("Error");
					a.setContentText(e.getMessage() + System.lineSeparator() + dmsg);
					a.show();

					pw.close();
					sw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
}
