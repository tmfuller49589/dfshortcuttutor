module modname {
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.base;
	requires org.apache.logging.log4j;
	requires javafx.graphics;

	opens ca.tfuller.dfshortcuttutor.gui;
	opens ca.tfuller.dfshortcuttutor.client;
	opens ca.tfuller.dfshortcuttutor.server;

}